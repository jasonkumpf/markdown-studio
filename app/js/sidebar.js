(function ($) {
  "use strict";

  const {remote} = require('electron');
  const {Menu, BrowserWindow, MenuItem, shell} = remote;

  const abar = require('address_bar');
  const folder_view = require('folder_view');

  var App = {
    // set path for file explorer
    setPath: function (path) {
      if (path.indexOf('~') == 0) {
        path = path.replace('~', process.env['HOME']);
      }
      this.folder.open(path);
      this.addressbar.set(path);
    }
  };

  $(function() {

    var folder = new folder_view.Folder($('#files'));
    var addressbar = new abar.AddressBar($('#addressbar'));

    folder.open(process.cwd());
    addressbar.set(process.cwd());

    App.folder = folder;
    App.addressbar = addressbar;

    folder.on('navigate', function(dir, mime) {
      if (mime.type == 'folder') {
        addressbar.enter(mime);
      } else {
        shell.openItem(mime.path);
      }
    });

    addressbar.on('navigate', function(dir) {
      folder.open(dir);
    });
  });

})(jQuery);
