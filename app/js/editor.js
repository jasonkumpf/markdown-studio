(function ($) {
  "use strict";

  var newButton, openButton, saveButton;
  var editor, converter, menu;
  var fileEntry;
  var hasWriteAccess;

  const {remote, clipboard} = require('electron');
  const {Menu, BrowserWindow, MenuItem, dialog, shell } = remote;
  const fs = require("fs");

  const abar = require('address_bar');
  const folder_view = require('folder_view');

  var App = {
    // set path for file explorer
    setPath: function (path) {
      if (path.indexOf('~') == 0) {
        path = path.replace('~', process.env['HOME']);
      }
      this.folder.open(path);
      this.addressbar.set(path);
    }
  };

  // Really not the best way to do things as it includes Markdown formatting along with words
	function updateWordCount() {
		var wordCount = document.getElementsByClassName('entry-word-count')[0],
			editorValue = editor.getValue();

		if (editorValue.length) {
			wordCount.innerHTML = editorValue.match(/\S+/g).length + ' words';
		}
	}

	function updateImagePlaceholders(content) {
		var imgPlaceholders = $(document.getElementsByClassName('rendered-markdown')[0]).find('p').filter(function() {
			return (/^(?:\{<(.*?)>\})?!(?:\[([^\n\]]*)\])(?:\(([^\n\]]*)\))?$/gim).test($(this).text());
		});

		$(imgPlaceholders).each(function( index ) {

			var elemindex = index,
				self = $(this),
				altText = self.text();

			(function(){

				self.dropzone({
					url: "/article/imgupload",
					success: function( file, response ){
						var holderP = $(file.previewElement).closest("p"),

							// Update the image path in markdown
							imgHolderMardown = $(".CodeMirror-code").find('pre').filter(function() {
						    	return (/^(?:\{<(.*?)>\})?!(?:\[([^\n\]]*)\])(?:\(([^\n\]]*)\))?$/gim).test(self.text()) && (self.find("span").length === 0);
							}),

							// Get markdown
							editorOrigVal = editor.getValue(),
							nth = 0,
							newMarkdown = editorOrigVal.replace(/^(?:\{<(.*?)>\})?!(?:\[([^\n\]]*)\])(:\(([^\n\]]*)\))?$/gim, function (match, i, original){
								nth++;
								return (nth === (elemindex+1)) ? (match + "(" + response.path +")") : match;
							});
							editor.setValue( newMarkdown );

						// Set image instead of placeholder
						holderP.removeClass("dropzone").html('<img src="'+ response.path +'"/>');
					}
				}).addClass("dropzone");
			}());
		})
	}

	function updatePreview() {
		var preview = document.getElementsByClassName('rendered-markdown')[0];
		preview.innerHTML = converter.makeHtml(editor.getValue());

		updateImagePlaceholders(preview.innerHTML);
		updateWordCount();
	}

  function newFile() {
    fileEntry = null;
    hasWriteAccess = false;
  }

  function setFile(theFileEntry, isWritable) {
    fileEntry = theFileEntry;
    hasWriteAccess = isWritable;
  }

  function readFileIntoEditor(theFileEntry) {
    fs.readFile(theFileEntry.toString(), function (err, data) {
      if (err) {
        console.log("Read failed: " + err);
      }

      editor.setValue(String(data));
    });
  }

  function writeEditorToFile(theFileEntry) {
    var str = editor.getValue();
    fs.writeFile(theFileEntry, editor.getValue(), function (err) {
      if (err) {
        console.log("Write failed: " + err);
        return;
      }
    });
  }

  var onChosenFileToOpen = function(theFileEntry) {
    console.log(theFileEntry);
    setFile(theFileEntry, false);
    readFileIntoEditor(theFileEntry);
  };

  var onChosenFileToSave = function(theFileEntry) {
    setFile(theFileEntry, true);
    writeEditorToFile(theFileEntry);
  };

  function handleNewButton() {
    if (false) {
      newFile();
      editor.setValue("");
    } else {
      window.open('file://' + __dirname + '/index.html');
    }
  }

  function handleOpenButton() {
    dialog.showOpenDialog({properties: ['openFile']}, function(filename) {
        onChosenFileToOpen(filename.toString());
    });
  }

  function handleSaveButton() {
    if (fileEntry && hasWriteAccess) {
      writeEditorToFile(fileEntry);
    } else {
      dialog.showSaveDialog(function(filename) {
         onChosenFileToSave(filename.toString(), true);
      });
    }
  }

  $(function() {

    if (!document.getElementById('entry-markdown'))
      return;

    //var delay;
    converter = new showdown.Converter();
    editor = CodeMirror.fromTextArea(document.getElementById('entry-markdown'), {
        mode: 'markdown',
        tabMode: 'indent',
        lineWrapping: true
    });

    $('.entry-markdown header, .entry-preview header').click(function (e) {
        $('.entry-markdown, .entry-preview').removeClass('active');
        $(e.target).closest('section').addClass('active');
    });
    $("#new").on('click', function() {
      handleNewButton();
    });
    $("#open").on('click', function() {
      handleOpenButton();
    });
    $("#save").on('click', function() {
      handleSaveButton();
    });
    $("#saveas").on('click', function() {
      setFile(fileEntry, false);
      handleSaveButton();
    });

    var nav_folders = $( "#files" );
    var folder = new folder_view.Folder(nav_folders);
    var addressbar = new abar.AddressBar($('#addressbar'));

    folder.open(process.cwd());
    addressbar.set(process.cwd());

    App.folder = folder;
    App.addressbar = addressbar;

    folder.on('navigate', function(dir, mime) {
      if (mime.type == 'folder') {
        if (mime.path == '../') {
          addressbar.exit(mime);          
        } else {
          addressbar.enter(mime);
        }
      } else {
        //shell.openItem(mime.path);
        onChosenFileToOpen(mime.path);
        setFile(mime.path, true);
      }
    });

    addressbar.on('navigate', function(dir) {
      folder.open(dir);
    });

    editor.on("change", function () {
        updatePreview();
    });

    updatePreview();

    // Sync scrolling
    function syncScroll(e) {
        // vars
        var $codeViewport = $(e.target),
            $previewViewport = $('.entry-preview-content'),
            $codeContent = $('.CodeMirror-sizer'),
            $previewContent = $('.rendered-markdown'),

        // calc position
            codeHeight = $codeContent.height() - $codeViewport.height(),
            previewHeight = $previewContent.height() - $previewViewport.height(),
            ratio = previewHeight / codeHeight,
            previewPostition = $codeViewport.scrollTop() * ratio;

        // apply new scroll
        $previewViewport.scrollTop(previewPostition);
    }

    // TODO: Debounce
    $('.CodeMirror-scroll').on('scroll', syncScroll);

    // Shadow on Markdown if scrolled
    $('.CodeMirror-scroll').scroll(function() {
        if ($('.CodeMirror-scroll').scrollTop() > 10) {
            $('.entry-markdown').addClass('scrolling');
        } else {
            $('.entry-markdown').removeClass('scrolling');
        }
    });
    // Shadow on Preview if scrolled
    $('.entry-preview-content').scroll(function() {
        if ($('.entry-preview-content').scrollTop() > 10) {
            $('.entry-preview').addClass('scrolling');
        } else {
            $('.entry-preview').removeClass('scrolling');
        }
    });

  });

}(jQuery));
